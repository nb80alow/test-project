FROM public.ecr.aws/docker/library/python:3.9.16-bullseye
WORKDIR /Uni/Master/Software_Engineering_für_KI-Systeme/Übung/test-project
COPY hello_world.py /Uni/Master/Software_Engineering_für_KI-Systeme/Übung/test-project
CMD ["python", "hello_world.py"]
