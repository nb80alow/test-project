# %% [markdown]
# # Language identification model
#
# In this exercise you will fill in the gaps to build a model that takes text as an input an predicts the language.
# This notebook will guide you through all steps that have to be performed but expects you to have basic knowledge of TensorFlow. If you are unsure, you can consult the notebook containing an introduction to TensorFlow.
#
#
# ### Objectives
#
# * Perform a simple exploration over a text dataset of language identification
# * Build a simple neural network for automatic identification of language (english, spanish and german)
#
# ### Requirements
#
# For installing all necessar requirements use the language_identification.yml and the following commands
# ```
# conda env create -f language_identification.yaml
# conda activate language_identification
# ```
# For installing a kernel for running this notebook create a new ipython kernel within the activated environment
# ```
# python -m ipykernel install --user --name=language_detection
# ```
#

# %%
import sys
import os
import requests

import nltk
import sklearn
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import numpy as np


nltk.download("stopwords")
nltk.download("punkt")

print(tf.__version__)

# %% [markdown]
# # Data importation
#
# In this step, we will use a dataset of 90k samples consisting of text passages and their corresponding language label. This dataset contains samples from 20 different languages. However for our example we will focus on 3 different languages: English, Spanish and German. [Further information can be found here](https://huggingface.co/datasets/papluca/language-identification)
#
# <mark>Step 1.</mark> Create a directory for your data named ```data```
#

# %%
# Download data and save it into the data folder
DATA_DIRECTORY = "data"

with open(f"{DATA_DIRECTORY}/train.csv", "wb") as f:
    f.write(
        requests.get(
            "https://huggingface.co/datasets/papluca/language-identification/resolve/main/train.csv"
        ).content
    )

with open(f"{DATA_DIRECTORY}/test.csv", "wb") as f:
    f.write(
        requests.get(
            "https://huggingface.co/datasets/papluca/language-identification/resolve/main/test.csv"
        ).content
    )

with open(f"{DATA_DIRECTORY}/valid.csv", "wb") as f:
    f.write(
        requests.get(
            "https://huggingface.co/datasets/papluca/language-identification/resolve/main/valid.csv"
        ).content
    )


# %% [markdown]
# <mark>Step 2.</mark> Load the data into a train, validation and testset using pandas
#

# %%
# Load data in pandas for filtering

train_df = pd.read_csv("data/train.csv")
val_df = pd.read_csv("data/valid.csv")
test_df = pd.read_csv("data/test.csv")

# %% [markdown]
# <mark>Step 3.</mark> Filter the dataset to contain only data for the languages we are interested in (English, Spanish, German)
#

# %%
# Select only "en", "es" and "de"
lang_list = ["es", "en", "de"]

train_df = train_df.loc[train_df.labels.isin(lang_list)]
val_df = val_df.loc[val_df.labels.isin(lang_list)]
test_df = test_df.loc[test_df.labels.isin(lang_list)]

train_df

# %% [markdown]
# # Data exploration
#
# In this step, we will perform a simple analysis of some important characteristics of our dataset:
#
# * Word frequency distribution
# * Distribution of labels
# * Text passage length distribution

# %%
# We calculate a list of words (tokens) which will be used for evaluate the distribution of
# tokens in the dataset

# We declare a stoplist for the three used languages
stoplist = [
    nltk.corpus.stopwords.words(lang) for lang in ["english", "spanish", "german"]
]
stoplist = set([word for lang_list in stoplist for word in lang_list])

# Now, only for visualization purposes we create a listh with all the tokens
word_list = []
for sentence in train_df["text"].to_list():
    word_list += [
        word for word in nltk.tokenize.word_tokenize(sentence) if word not in stoplist
    ]

print(f"Our corpus consists of {len(word_list)} different words.")


plt.figure(figsize=(8, 6))

fd = nltk.probability.FreqDist(word_list)
fd.plot(30)

print("Distribution of labels:")

train_df.labels.value_counts()

# %% [markdown]
# ## Exploring the length of the text sequences
#
# Before we define a base architecture in TensorFlow, we must **find a good value for the size of the input** of the model. We evaluate the average size (e.g. number of words/tokens) of each sample of the corpus.
#
# <mark>Step 4.</mark> Generate a `pandas.Series` Object that contains the length of each sentence in terms of tokens.
#

# %%
# We then evaluate the size of each sentence in the corpus

splitted_text = train_df["text"].apply(lambda txt: txt.split(" "))
splitted_text_len = splitted_text.apply(lambda x: len(x))
ax = splitted_text_len.plot.hist(bins=12, alpha=0.5)

print("Average length: {}".format(splitted_text_len.mean()))
print("Maximum length: {}".format(splitted_text_len.max()))
print("Standard deviation length: {}".format(splitted_text_len.std()))

# %% [markdown]
# We select $50$, as it covers up to 68% of the data (one standard deviation). Sequences longer than $50$ words will be chunked. **This is done for the sake of simplicity**

# %% [markdown]
# # Data preparation
# ## Using `tf.data` for creating the dataset
#
# `tf.data` is one of the most useful libraries for accessing datasets in TensorFlow. It provides several handlers for reading and creating usable datasets with low memory footprint.
#
#
# <mark>Step 5.</mark> Use `tf.keras.utils.to_categorical()` and the `sklearn.preprocessing.LabelEncoder.transform` functionality to  transorm the labels into integers (categorical data).
# * Build the datasets using `tf.data`
# * Transform the text input into sequences of up to 50 integers (vectorization)

# %%
# Create dictionary for enconding labels
# This is done using LabelEncoder from scikit-learn
# It's quite useful when the number if classes is high

le = sklearn.preprocessing.LabelEncoder()
le.fit(lang_list)

num_classes = len(le.classes_)

train_labels = tf.keras.utils.to_categorical(
    le.transform(train_df.pop("labels")), num_classes=num_classes
)
val_labels = tf.keras.utils.to_categorical(
    le.transform(val_df.pop("labels")), num_classes=num_classes
)
test_labels = tf.keras.utils.to_categorical(
    le.transform(test_df.pop("labels")), num_classes=num_classes
)

# %% [markdown]
# <mark>Step 6.</mark> Build the datasets using `tf.data.Dataset.from_tensor_slices` and pass your X data as a `list` and the, labels that were generated in the prior cell

# %%
raw_train_ds = tf.data.Dataset.from_tensor_slices(
    (train_df["text"].to_list(), train_labels)
)  # X, y
raw_val_ds = tf.data.Dataset.from_tensor_slices((val_df["text"].to_list(), val_labels))
raw_test_ds = tf.data.Dataset.from_tensor_slices(
    (test_df["text"].to_list(), test_labels)
)

# %%
# Exploring a sample of the corpus this should output a byte string and a Label array with 3 values of which only one is zero.

batch_size = 32
seed = 42

for text, label in raw_train_ds.take(1):
    print("Text: ", text.numpy())
    print("Label:", label.numpy())

# %% [markdown]
# At this point, the dataset is still raw, which means, it's still composed of text sequences.

# %% [markdown]
# ## Preprocessing the dataset using `tf.keras.layers.TextVectorization`
#
# Before the dataset can be fed to the model, the data has to be transformed into a vectorized representation of each text. This means each word will be transformed to an integer value e.g. ("book" --> 45).
#
# <mark>Step 7.</mark> Use `tf.keras.layers.TextVectorization` to generate a vectorization layer. Use `"lower_and_strip_punctuation"` as standardization and `int` as output mode.

# %%
# Prepare dataset for training

max_features = 10000  # top 10K most frequent words
sequence_length = 50  # Was defined in the previous data exploration section

vectorize_layer = tf.keras.layers.TextVectorization(
    standardize="lower_and_strip_punctuation",
    max_tokens=max_features,
    output_mode="int",
    output_sequence_length=sequence_length,
)

# This step is important to perform it only on the training set. The `adapt()` method will learn the vocabulary for our dataset

vectorize_layer.adapt(
    train_df["text"].to_list()
)  # vectorize layer is fitted to the training data

# %% [markdown]
# The following cell will visualize how the input to the model currently looks like.


# %%
# Helper for visualizing the processed input
def vectorize_text(text, label):
    text = tf.expand_dims(text, -1)
    return vectorize_layer(text), label


# Reviewing how a sample of the corpus will be fed to the model

text_batch, label_batch = next(iter(raw_train_ds.batch(64)))
first_review, first_label = text_batch[0], label_batch[0]
print("First text: ", first_review)
print("Language (label)", le.inverse_transform([np.argmax(first_label)]))
print("Vectorized text", vectorize_text(first_review, first_label))

# %% [markdown]
# <mark>Step 8.</mark> Create the final datasets using the vectorize layer. Use the `.map` function on the raw datasets, and return the vectorized text and corresponding y value.

# %%
# Create the final datasets using the vectorize_layer

train_ds = raw_train_ds.map(
    lambda x, y: (vectorize_layer(x), y)
)  # returns vectorize_layer(text), label
val_ds = raw_val_ds.map(lambda x, y: (vectorize_layer(x), y))
test_ds = raw_test_ds.map(lambda x, y: (vectorize_layer(x), y))

# %%
# Applying cache techniques for improving inference and training time
# It allows tensorflow to prepare the data while it trains the model

AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.batch(batch_size=batch_size)
train_ds = train_ds.prefetch(AUTOTUNE)
val_ds = val_ds.batch(batch_size=batch_size)
val_ds = val_ds.prefetch(AUTOTUNE)
test_ds = test_ds.batch(batch_size=batch_size)
test_ds = test_ds.prefetch(AUTOTUNE)

# %% [markdown]
# # Model training and specification
#
# <mark>Step 9.</mark> Define the following architecture using `tf.keras.Sequential`:
# 1. `tf.keras.layers.Embedding` layer for building a more dense and compact representation for each of the top 10.000 most frequent words. Use the maximal number of features + 1 and 16 as the input and output dimensions.
# 2. `tf.keras.layers.Dropout` layer for improving regularization, use a dropout rate of 0.2
# 3. `tf.keras.layers.GlobalAveragePooling1D` for returning a fixed-length ouput vector for each example. It averages over the sequence dimension, allowing the model to handle variable size inputs (less than 50) in a simple way.
# 4. `tf.keras.layers.Dense` layer for the logits of each class (es, en, de)

# %%
# Model training

model = tf.keras.Sequential(
    [
        tf.keras.layers.Embedding(max_features + 1, 16),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.GlobalAveragePooling1D(),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(3),
    ]
)

model.summary()
tf.keras.utils.plot_model(model=model, rankdir="LR", dpi=72, show_shapes=True)


# %% [markdown]
#
# <mark>Step 10.</mark> Compile the model using the following settings:
# * `tf.keras.losses.CategoricalCrossentropy` with 0.1 for label smoothing
# * Adam as optimizer
# * Accuracy as metric
#

# %%
model.compile(
    loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True, label_smoothing=0.1),
    optimizer="adam",
    metrics=["accuracy"],
)

# %% [markdown]
#
# <mark>Step 11.</mark> Train the model for 10 epochs
#

# %%
# Train

history = model.fit(train_ds, validation_data=val_ds, epochs=10)

# %% [markdown]
# # Model evaluation
#
#
# <mark>Step 12.</mark> Evaluate the model on the test set
#

# %%
# Evaluate the model

loss, accuracy = model.evaluate(test_ds)

print("Loss: ", loss)
print("Accuracy: ", accuracy)

# %%
# Plot loss
history_dict = history.history
print("Keys of the history variable")
print(history_dict.keys())

acc = history_dict["accuracy"]
val_acc = history_dict["val_accuracy"]
loss = history_dict["loss"]
val_loss = history_dict["val_loss"]

epochs = range(1, len(acc) + 1)

plt.plot(epochs, loss, "r", label="Training loss")
plt.plot(epochs, val_loss, "b", label="Validation loss")
plt.title("Training and validation loss")
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend()

plt.show()


# %%
# Plot accuracy

plt.plot(epochs, acc, "r", label="Training acc")
plt.plot(epochs, val_acc, "b", label="Validation acc")
plt.title("Training and validation accuracy")
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.legend(loc="lower right")

plt.show()

# %% [markdown]
# <mark>Step 13.</mark> Fill in the softmax function


# %%
def softmax(x):
    """x is a numpy array where each row corresponds to the scores (outputs) of the model,
    e.g.: [[0.2, 0.6, 0.7], [0.1, 0.5, 0.4]]
    Compute softmax values for each sets of scores in x."""
    return np.exp(x) / np.sum(np.exp(x), axis=0)


# %% [markdown]
# ### Predict on unseen data
# <mark>Step 14.</mark> Use the vectorize layer to prepare your inputs.

# %%

examples = [
    "Esto me pareció increíble",
    "I think it was incredible.",
    "Ich finde es war unglaublich",
]

examples_vectorized = vectorize_layer(examples)


# %% [markdown]
# <mark>Step 15.</mark> Use your model to predict the logits of the vectorized examples

# %%
logits = model.predict(examples_vectorized)
probits = softmax(logits)
idx_predictions = np.argmax(probits, axis=1)
print("Probabilities: {}".format(np.max(probits, axis=1)))
print("Corresponding classes: {}".format(le.inverse_transform(idx_predictions)))

# %% [markdown]
# <mark>Step 16.</mark> Create a directory called `saved_model` and save the model using `.save()`

# %%
model.save("saved_model/simple_mlp_novectorize.h5")

# %% [markdown]
# The next cell stores the vectorize layer in a directory called "vectorizer". As TF yet does not support saving TextVectorizer objects, we save and load it as a layer of tf.keras.Sequential
#

# %%


def store_text_vectorizer(vectorizer, file_path: str):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.Input(shape=(1,), dtype=tf.string))
    model.add(vectorizer)
    model.save(file_path)


store_text_vectorizer(vectorize_layer, "vectorizer")


# %% [markdown]
# <mark>Step 17.</mark> Load the vectorize layer and model. You can load the vectorize layer by loading it as a `tf.model` and then access the first layer using `.layers[0]`

# %%
# Load the model
saved_model = tf.keras.models.load_model("saved_model/simple_mlp_novectorize.h5")
vectorize_layer = tf.keras.models.load_model("vectorizer").layers[0]

new_examples = ["I made it to task 3.", "Ich hab es bis task 3 geschafft."]

new_examples_vectorized = vectorize_layer(new_examples)
logits = saved_model.predict(new_examples_vectorized)

probabilities = softmax(logits)
print(probabilities)
for label, text in zip(probabilities, new_examples):
    print(text, le.inverse_transform([np.argmax(label)]))
